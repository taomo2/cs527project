# The following dependencies were calculated from:
#
# generate_workspace --maven_project=/Users/taomo/Documents/workspace/bazelProjects/commons-functor/core -o=/Users/taomo/Documents/workspace/bazelProjects/commons-functor/core


def generated_maven_jars():
  # pom.xml got requested version
  # org.apache.commons:commons-functor:jar:1.0-SNAPSHOT
  native.maven_jar(
      name = "org_apache_commons_commons_lang3",
      artifact = "org.apache.commons:commons-lang3:3.1",
      sha1 = "905075e6c80f206bbe6cf1e809d2caa69f420c76",
  )


  # junit:junit:jar:4.10
  native.maven_jar(
      name = "org_hamcrest_hamcrest_core",
      artifact = "org.hamcrest:hamcrest-core:1.1",
      sha1 = "860340562250678d1a344907ac75754e259cdb14",
  )


  # pom.xml got requested version
  # org.apache.commons:commons-functor:jar:1.0-SNAPSHOT
  native.maven_jar(
      name = "junit_junit",
      artifact = "junit:junit:4.10",
      sha1 = "e4f1766ce7404a08f45d859fb9c226fc9e41a861",
  )




def generated_java_libraries():
  native.java_library(
      name = "org_apache_commons_commons_lang3",
      visibility = ["//visibility:public"],
      exports = ["@org_apache_commons_commons_lang3//jar"],
  )


  native.java_library(
      name = "org_hamcrest_hamcrest_core",
      visibility = ["//visibility:public"],
      exports = ["@org_hamcrest_hamcrest_core//jar"],
  )


  native.java_library(
      name = "junit_junit",
      visibility = ["//visibility:public"],
      exports = ["@junit_junit//jar"],
      runtime_deps = [
          ":org_hamcrest_hamcrest_core",
      ],
  )


