# The following dependencies were calculated from:
#
# generate_workspace --maven_project=/Users/taomo/Documents/workspace/bazelProjects/exampleMultiModule/parent-proje -o=/Users/taomo/Documents/workspace/bazelProjects/exampleMultiModule/parent-proje


def generated_maven_jars():
  # pom.xml got requested version
  # com.websystique.multimodule:parent-proje:pom:1.0-SNAPSHOT
  native.maven_jar(
      name = "junit_junit",
      artifact = "junit:junit:3.8.1",
      sha1 = "99129f16442844f6a4a11ae22fbbee40b14d774f",
  )




def generated_java_libraries():
  native.java_library(
      name = "junit_junit",
      visibility = ["//visibility:public"],
      exports = ["@junit_junit//jar"],
  )


