# The following dependencies were calculated from:
#
# generate_workspace --maven_project=/Users/taomo/Documents/workspace/bazelProjects/fileUpload/commons-fileupload --output_dir=/Users/taomo/Documents/workspace/bazelProjects/fileUpload/commons-fileupload/bazel_generate_workspace_files


def generated_maven_jars():
  # junit:junit:jar:4.12
  native.maven_jar(
      name = "org_hamcrest_hamcrest_core",
      artifact = "org.hamcrest:hamcrest-core:1.3",
      sha1 = "42a25dc3219429f0e5d060061f71acb49bf010a0",
  )


  # pom.xml got requested version
  # commons-fileupload:commons-fileupload:jar:1.4-SNAPSHOT
  native.maven_jar(
      name = "commons_io_commons_io",
      artifact = "commons-io:commons-io:2.2",
      sha1 = "83b5b8a7ba1c08f9e8c8ff2373724e33d3c1e22a",
  )


  # pom.xml got requested version
  # commons-fileupload:commons-fileupload:jar:1.4-SNAPSHOT
  native.maven_jar(
      name = "javax_servlet_servlet_api",
      artifact = "javax.servlet:servlet-api:2.4",
      sha1 = "3fc542fe8bb8164e8d3e840fe7403bc0518053c0",
  )


  # pom.xml got requested version
  # commons-fileupload:commons-fileupload:jar:1.4-SNAPSHOT
  native.maven_jar(
      name = "portlet_api_portlet_api",
      artifact = "portlet-api:portlet-api:1.0",
      sha1 = "f74ea066f0ba1e40a48ca54700bc7430aacebf8d",
  )


  # pom.xml got requested version
  # commons-fileupload:commons-fileupload:jar:1.4-SNAPSHOT
  native.maven_jar(
      name = "junit_junit",
      artifact = "junit:junit:4.12",
      sha1 = "2973d150c0dc1fefe998f834810d68f278ea58ec",
  )




def generated_java_libraries():
  native.java_library(
      name = "org_hamcrest_hamcrest_core",
      visibility = ["//visibility:public"],
      exports = ["@org_hamcrest_hamcrest_core//jar"],
  )


  native.java_library(
      name = "commons_io_commons_io",
      visibility = ["//visibility:public"],
      exports = ["@commons_io_commons_io//jar"],
  )


  native.java_library(
      name = "javax_servlet_servlet_api",
      visibility = ["//visibility:public"],
      exports = ["@javax_servlet_servlet_api//jar"],
  )


  native.java_library(
      name = "portlet_api_portlet_api",
      visibility = ["//visibility:public"],
      exports = ["@portlet_api_portlet_api//jar"],
  )


  native.java_library(
      name = "junit_junit",
      visibility = ["//visibility:public"],
      exports = ["@junit_junit//jar"],
      runtime_deps = [
          ":org_hamcrest_hamcrest_core",
      ],
  )


