# The following dependencies were calculated from:
#
# generate_workspace --maven_project=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/debugger/ -o=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/debugger/generatedbzl


def generated_maven_jars():
  # pom.xml got requested version
  # org.apache.pdfbox:pdfbox-debugger:jar:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "com_github_jai_imageio_jai_imageio_jpeg2000",
      artifact = "com.github.jai-imageio:jai-imageio-jpeg2000:1.3.0",
      sha1 = "94cba6faa03d2a279d00b284b76231ae2f2f67e8",
  )


  # pom.xml got requested version
  # com.github.jai-imageio:jai-imageio-jpeg2000:jar:1.3.0 wanted version 1.3.0
  # org.apache.pdfbox:pdfbox-debugger:jar:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "com_github_jai_imageio_jai_imageio_core",
      artifact = "com.github.jai-imageio:jai-imageio-core:1.3.1",
      sha1 = "300f3f0dedfca37b3278991f9397354971b37156",
  )


  # pom.xml got requested version
  # org.apache.pdfbox:pdfbox-debugger:jar:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "com_levigo_jbig2_levigo_jbig2_imageio",
      artifact = "com.levigo.jbig2:levigo-jbig2-imageio:2.0",
      sha1 = "d67a342fe6d81928cb5596832a02f6809565470d",
  )




def generated_java_libraries():
  native.java_library(
      name = "com_github_jai_imageio_jai_imageio_jpeg2000",
      visibility = ["//visibility:public"],
      exports = ["@com_github_jai_imageio_jai_imageio_jpeg2000//jar"],
      runtime_deps = [
          ":com_github_jai_imageio_jai_imageio_core",
      ],
  )


  native.java_library(
      name = "com_github_jai_imageio_jai_imageio_core",
      visibility = ["//visibility:public"],
      exports = ["@com_github_jai_imageio_jai_imageio_core//jar"],
  )


  native.java_library(
      name = "com_levigo_jbig2_levigo_jbig2_imageio",
      visibility = ["//visibility:public"],
      exports = ["@com_levigo_jbig2_levigo_jbig2_imageio//jar"],
  )


