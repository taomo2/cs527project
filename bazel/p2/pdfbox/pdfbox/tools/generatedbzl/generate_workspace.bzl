# The following dependencies were calculated from:
#
# generate_workspace --maven_project=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/tools/ -o=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/tools/generatedbzl


def generated_maven_jars():
  # pom.xml got requested version
  # org.apache.pdfbox:pdfbox-tools:jar:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "com_github_jai_imageio_jai_imageio_jpeg2000",
      artifact = "com.github.jai-imageio:jai-imageio-jpeg2000:1.3.0",
      sha1 = "94cba6faa03d2a279d00b284b76231ae2f2f67e8",
  )


  # junit:junit:jar:4.12
  native.maven_jar(
      name = "org_hamcrest_hamcrest_core",
      artifact = "org.hamcrest:hamcrest-core:1.3",
      sha1 = "42a25dc3219429f0e5d060061f71acb49bf010a0",
  )


  # pom.xml got requested version
  # com.github.jai-imageio:jai-imageio-jpeg2000:jar:1.3.0 wanted version 1.3.0
  # org.apache.pdfbox:pdfbox-tools:jar:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "com_github_jai_imageio_jai_imageio_core",
      artifact = "com.github.jai-imageio:jai-imageio-core:1.3.1",
      sha1 = "300f3f0dedfca37b3278991f9397354971b37156",
  )


  # pom.xml got requested version
  # org.apache.pdfbox:pdfbox-tools:jar:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "com_levigo_jbig2_levigo_jbig2_imageio",
      artifact = "com.levigo.jbig2:levigo-jbig2-imageio:2.0",
      sha1 = "d67a342fe6d81928cb5596832a02f6809565470d",
  )


  # pom.xml got requested version
  # org.apache.pdfbox:pdfbox-tools:jar:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "junit_junit",
      artifact = "junit:junit:4.12",
      sha1 = "2973d150c0dc1fefe998f834810d68f278ea58ec",
  )




def generated_java_libraries():
  native.java_library(
      name = "com_github_jai_imageio_jai_imageio_jpeg2000",
      visibility = ["//visibility:public"],
      exports = ["@com_github_jai_imageio_jai_imageio_jpeg2000//jar"],
      runtime_deps = [
          ":com_github_jai_imageio_jai_imageio_core",
      ],
  )


  native.java_library(
      name = "org_hamcrest_hamcrest_core",
      visibility = ["//visibility:public"],
      exports = ["@org_hamcrest_hamcrest_core//jar"],
  )


  native.java_library(
      name = "com_github_jai_imageio_jai_imageio_core",
      visibility = ["//visibility:public"],
      exports = ["@com_github_jai_imageio_jai_imageio_core//jar"],
  )


  native.java_library(
      name = "com_levigo_jbig2_levigo_jbig2_imageio",
      visibility = ["//visibility:public"],
      exports = ["@com_levigo_jbig2_levigo_jbig2_imageio//jar"],
  )


  native.java_library(
      name = "junit_junit",
      visibility = ["//visibility:public"],
      exports = ["@junit_junit//jar"],
      runtime_deps = [
          ":org_hamcrest_hamcrest_core",
      ],
  )


