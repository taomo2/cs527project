# The following dependencies were calculated from:
#
# generate_workspace --maven_project=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/preflight/ -o=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/preflight/generatedbzl


def generated_maven_jars():
  # junit:junit:jar:4.12
  native.maven_jar(
      name = "org_hamcrest_hamcrest_core",
      artifact = "org.hamcrest:hamcrest-core:1.3",
      sha1 = "42a25dc3219429f0e5d060061f71acb49bf010a0",
  )


  # pom.xml got requested version
  # org.apache.pdfbox:preflight:bundle:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "log4j_log4j",
      artifact = "log4j:log4j:1.2.17",
      sha1 = "5af35056b4d257e4b64b9e8069c0746e8b08629f",
  )


  # pom.xml got requested version
  # org.apache.pdfbox:preflight:bundle:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "commons_io_commons_io",
      artifact = "commons-io:commons-io:2.5",
      sha1 = "2852e6e05fbb95076fc091f6d1780f1f8fe35e0f",
  )


  # pom.xml got requested version
  # org.apache.pdfbox:preflight:bundle:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "com_levigo_jbig2_levigo_jbig2_imageio",
      artifact = "com.levigo.jbig2:levigo-jbig2-imageio:2.0",
      sha1 = "d67a342fe6d81928cb5596832a02f6809565470d",
  )


  # pom.xml got requested version
  # org.apache.pdfbox:preflight:bundle:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "junit_junit",
      artifact = "junit:junit:4.12",
      sha1 = "2973d150c0dc1fefe998f834810d68f278ea58ec",
  )




def generated_java_libraries():
  native.java_library(
      name = "org_hamcrest_hamcrest_core",
      visibility = ["//visibility:public"],
      exports = ["@org_hamcrest_hamcrest_core//jar"],
  )


  native.java_library(
      name = "log4j_log4j",
      visibility = ["//visibility:public"],
      exports = ["@log4j_log4j//jar"],
  )


  native.java_library(
      name = "commons_io_commons_io",
      visibility = ["//visibility:public"],
      exports = ["@commons_io_commons_io//jar"],
  )


  native.java_library(
      name = "com_levigo_jbig2_levigo_jbig2_imageio",
      visibility = ["//visibility:public"],
      exports = ["@com_levigo_jbig2_levigo_jbig2_imageio//jar"],
  )


  native.java_library(
      name = "junit_junit",
      visibility = ["//visibility:public"],
      exports = ["@junit_junit//jar"],
      runtime_deps = [
          ":org_hamcrest_hamcrest_core",
      ],
  )


