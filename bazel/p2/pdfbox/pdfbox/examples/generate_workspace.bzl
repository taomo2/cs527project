# The following dependencies were calculated from:
#
# generate_workspace --maven_project=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/examples/ -o=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/examples/generatedbzl


def generated_maven_jars():
  # org.slf4j:jcl-over-slf4j:jar:1.6.1
  native.maven_jar(
      name = "org_slf4j_slf4j_api",
      artifact = "org.slf4j:slf4j-api:1.6.1",
      sha1 = "6f3b8a24bf970f17289b234284c94f43eb42f0e4",
  )


  # pom.xml got requested version
  # org.apache.pdfbox:pdfbox-examples:jar:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "org_apache_lucene_lucene_analyzers_common",
      artifact = "org.apache.lucene:lucene-analyzers-common:5.5.4",
      sha1 = "08ce9d34c8124c80e176e8332ee947480bbb9576",
  )


  # org.springframework:spring-web:jar:3.0.5.RELEASE got requested version
  # org.springframework:spring-aop:jar:3.0.5.RELEASE
  native.maven_jar(
      name = "aopalliance_aopalliance",
      artifact = "aopalliance:aopalliance:1.0",
      sha1 = "0235ba8b489512805ac13a8f9ea77a1ca5ebe3e8",
  )


  # org.apache.ant:ant:jar:1.9.9
  native.maven_jar(
      name = "org_apache_ant_ant_launcher",
      artifact = "org.apache.ant:ant-launcher:1.9.9",
      sha1 = "c5841b18f5299f17fc53223c3a378e08278a5ef7",
  )


  # org.apache.wink:wink-component-test-support:jar:1.4
  # org.springframework:spring-web:jar:3.0.5.RELEASE got requested version
  native.maven_jar(
      name = "org_springframework_spring_context",
      artifact = "org.springframework:spring-context:3.0.5.RELEASE",
      sha1 = "6b05e397566cc7750d2d25f81a7441fe1aeecb75",
  )


  # junit:junit:jar:4.12
  native.maven_jar(
      name = "org_hamcrest_hamcrest_core",
      artifact = "org.hamcrest:hamcrest-core:1.3",
      sha1 = "42a25dc3219429f0e5d060061f71acb49bf010a0",
  )


  # org.springframework:spring-context:jar:3.0.5.RELEASE
  native.maven_jar(
      name = "org_springframework_spring_aop",
      artifact = "org.springframework:spring-aop:3.0.5.RELEASE",
      sha1 = "0c7a17803cc10512e26e285073639543f0c7c764",
  )


  # org.apache.wink:wink-component-test-support:jar:1.4
  native.maven_jar(
      name = "org_slf4j_jcl_over_slf4j",
      artifact = "org.slf4j:jcl-over-slf4j:1.6.1",
      sha1 = "99c61095a14dfc9e47a086068033c286bf236475",
  )


  # org.springframework:spring-context:jar:3.0.5.RELEASE
  native.maven_jar(
      name = "org_springframework_spring_expression",
      artifact = "org.springframework:spring-expression:3.0.5.RELEASE",
      sha1 = "5b8e53877cb58c94f15a0d8172da3569f4b4f3fb",
  )


  # org.springframework:spring-core:jar:3.0.5.RELEASE got requested version
  # org.springframework:spring-context:jar:3.0.5.RELEASE got requested version
  # org.springframework:spring-aop:jar:3.0.5.RELEASE
  native.maven_jar(
      name = "org_springframework_spring_asm",
      artifact = "org.springframework:spring-asm:3.0.5.RELEASE",
      sha1 = "07f22a0e9f325e6565b4ea56b479ad76311d146b",
  )


  # pom.xml got requested version
  # org.apache.pdfbox:pdfbox-examples:jar:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "org_apache_wink_wink_component_test_support",
      artifact = "org.apache.wink:wink-component-test-support:1.4",
      sha1 = "51c5abe27e9a5fa023118a21c00f461873ef13ff",
  )


  # org.apache.wink:wink-component-test-support:jar:1.4
  native.maven_jar(
      name = "org_springframework_spring_web",
      artifact = "org.springframework:spring-web:3.0.5.RELEASE",
      sha1 = "d5c550739c4c0cb7ca527ef46c9aed72fb215eee",
  )


  # org.springframework:spring-web:jar:3.0.5.RELEASE got requested version
  # org.springframework:spring-context:jar:3.0.5.RELEASE got requested version
  # org.springframework:spring-aop:jar:3.0.5.RELEASE
  native.maven_jar(
      name = "org_springframework_spring_beans",
      artifact = "org.springframework:spring-beans:3.0.5.RELEASE",
      sha1 = "4b352a9c3b427294e264ca4d460d07417ca9350e",
  )


  # org.apache.wink:wink-component-test-support:jar:1.4
  native.maven_jar(
      name = "org_apache_geronimo_specs_geronimo_jaxrs_1_1_spec",
      artifact = "org.apache.geronimo.specs:geronimo-jaxrs_1.1_spec:1.0",
      sha1 = "6f4c71cbff6a7725e393a74b9e3680d2685ddac7",
  )


  # org.springframework:spring-beans:jar:3.0.5.RELEASE
  # org.springframework:spring-web:jar:3.0.5.RELEASE got requested version
  # org.springframework:spring-context:jar:3.0.5.RELEASE got requested version
  # org.springframework:spring-aop:jar:3.0.5.RELEASE got requested version
  # org.springframework:spring-expression:jar:3.0.5.RELEASE got requested version
  native.maven_jar(
      name = "org_springframework_spring_core",
      artifact = "org.springframework:spring-core:3.0.5.RELEASE",
      sha1 = "1633e94943d57746ef76910489f1cd71fe667e04",
  )


  # org.apache.lucene:lucene-analyzers-common:jar:5.5.4 got requested version
  # pom.xml got requested version
  # org.apache.pdfbox:pdfbox-examples:jar:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "org_apache_lucene_lucene_core",
      artifact = "org.apache.lucene:lucene-core:5.5.4",
      sha1 = "ab9c77e75cf142aa6e284b310c8395617bd9b19b",
  )


  # pom.xml got requested version
  # org.apache.pdfbox:pdfbox-examples:jar:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "org_apache_ant_ant",
      artifact = "org.apache.ant:ant:1.9.9",
      sha1 = "9dc55233d8c0809e57b2ec7f78376da3f32872bd",
  )


  # pom.xml got requested version
  # org.apache.pdfbox:pdfbox-examples:jar:3.0.0-SNAPSHOT
  # org.apache.wink:wink-component-test-support:jar:1.4 wanted version 4.10
  native.maven_jar(
      name = "junit_junit",
      artifact = "junit:junit:4.12",
      sha1 = "2973d150c0dc1fefe998f834810d68f278ea58ec",
  )


  # org.apache.wink:wink-component-test-support:jar:1.4
  native.maven_jar(
      name = "xmlunit_xmlunit",
      artifact = "xmlunit:xmlunit:1.1",
      sha1 = "81aef0317b1e938b81ff11402037f3b33204aa16",
  )




def generated_java_libraries():
  native.java_library(
      name = "org_slf4j_slf4j_api",
      visibility = ["//visibility:public"],
      exports = ["@org_slf4j_slf4j_api//jar"],
  )


  native.java_library(
      name = "org_apache_lucene_lucene_analyzers_common",
      visibility = ["//visibility:public"],
      exports = ["@org_apache_lucene_lucene_analyzers_common//jar"],
      runtime_deps = [
          ":org_apache_lucene_lucene_core",
      ],
  )


  native.java_library(
      name = "aopalliance_aopalliance",
      visibility = ["//visibility:public"],
      exports = ["@aopalliance_aopalliance//jar"],
  )


  native.java_library(
      name = "org_apache_ant_ant_launcher",
      visibility = ["//visibility:public"],
      exports = ["@org_apache_ant_ant_launcher//jar"],
  )


  native.java_library(
      name = "org_springframework_spring_context",
      visibility = ["//visibility:public"],
      exports = ["@org_springframework_spring_context//jar"],
      runtime_deps = [
          ":aopalliance_aopalliance",
          ":org_springframework_spring_aop",
          ":org_springframework_spring_asm",
          ":org_springframework_spring_beans",
          ":org_springframework_spring_core",
          ":org_springframework_spring_expression",
      ],
  )


  native.java_library(
      name = "org_hamcrest_hamcrest_core",
      visibility = ["//visibility:public"],
      exports = ["@org_hamcrest_hamcrest_core//jar"],
  )


  native.java_library(
      name = "org_springframework_spring_aop",
      visibility = ["//visibility:public"],
      exports = ["@org_springframework_spring_aop//jar"],
      runtime_deps = [
          ":aopalliance_aopalliance",
          ":org_springframework_spring_asm",
          ":org_springframework_spring_beans",
          ":org_springframework_spring_core",
      ],
  )


  native.java_library(
      name = "org_slf4j_jcl_over_slf4j",
      visibility = ["//visibility:public"],
      exports = ["@org_slf4j_jcl_over_slf4j//jar"],
      runtime_deps = [
          ":org_slf4j_slf4j_api",
      ],
  )


  native.java_library(
      name = "org_springframework_spring_expression",
      visibility = ["//visibility:public"],
      exports = ["@org_springframework_spring_expression//jar"],
      runtime_deps = [
          ":org_springframework_spring_core",
      ],
  )


  native.java_library(
      name = "org_springframework_spring_asm",
      visibility = ["//visibility:public"],
      exports = ["@org_springframework_spring_asm//jar"],
  )


  native.java_library(
      name = "org_apache_wink_wink_component_test_support",
      visibility = ["//visibility:public"],
      exports = ["@org_apache_wink_wink_component_test_support//jar"],
      runtime_deps = [
          ":aopalliance_aopalliance",
          ":junit_junit",
          ":org_apache_geronimo_specs_geronimo_jaxrs_1_1_spec",
          ":org_slf4j_jcl_over_slf4j",
          ":org_slf4j_slf4j_api",
          ":org_springframework_spring_aop",
          ":org_springframework_spring_asm",
          ":org_springframework_spring_beans",
          ":org_springframework_spring_context",
          ":org_springframework_spring_core",
          ":org_springframework_spring_expression",
          ":org_springframework_spring_web",
          ":xmlunit_xmlunit",
      ],
  )


  native.java_library(
      name = "org_springframework_spring_web",
      visibility = ["//visibility:public"],
      exports = ["@org_springframework_spring_web//jar"],
      runtime_deps = [
          ":aopalliance_aopalliance",
          ":org_springframework_spring_beans",
          ":org_springframework_spring_context",
          ":org_springframework_spring_core",
      ],
  )


  native.java_library(
      name = "org_springframework_spring_beans",
      visibility = ["//visibility:public"],
      exports = ["@org_springframework_spring_beans//jar"],
      runtime_deps = [
          ":org_springframework_spring_asm",
          ":org_springframework_spring_core",
      ],
  )


  native.java_library(
      name = "org_apache_geronimo_specs_geronimo_jaxrs_1_1_spec",
      visibility = ["//visibility:public"],
      exports = ["@org_apache_geronimo_specs_geronimo_jaxrs_1_1_spec//jar"],
  )


  native.java_library(
      name = "org_springframework_spring_core",
      visibility = ["//visibility:public"],
      exports = ["@org_springframework_spring_core//jar"],
      runtime_deps = [
          ":org_springframework_spring_asm",
      ],
  )


  native.java_library(
      name = "org_apache_lucene_lucene_core",
      visibility = ["//visibility:public"],
      exports = ["@org_apache_lucene_lucene_core//jar"],
  )


  native.java_library(
      name = "org_apache_ant_ant",
      visibility = ["//visibility:public"],
      exports = ["@org_apache_ant_ant//jar"],
      runtime_deps = [
          ":org_apache_ant_ant_launcher",
      ],
  )


  native.java_library(
      name = "junit_junit",
      visibility = ["//visibility:public"],
      exports = ["@junit_junit//jar"],
      runtime_deps = [
          ":org_hamcrest_hamcrest_core",
      ],
  )


  native.java_library(
      name = "xmlunit_xmlunit",
      visibility = ["//visibility:public"],
      exports = ["@xmlunit_xmlunit//jar"],
  )


