# The following dependencies were calculated from:
#
# generate_workspace --maven_project=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/app/ -o=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/app/generatedbzl


def generated_maven_jars():
  # pom.xml got requested version
  # org.apache.pdfbox:pdfbox-app:bundle:3.0.0-SNAPSHOT
  native.maven_jar(
      name = "org_bouncycastle_bcmail_jdk15on",
      artifact = "org.bouncycastle:bcmail-jdk15on:1.57",
      sha1 = "61ddd0863a1fef1c7e7e45718cd7c13d5a661123",
  )


  # pom.xml got requested version
  # org.bouncycastle:bcmail-jdk15on:jar:1.57
  # org.apache.pdfbox:pdfbox-app:bundle:3.0.0-SNAPSHOT got requested version
  # org.bouncycastle:bcpkix-jdk15on:jar:1.57 got requested version
  native.maven_jar(
      name = "org_bouncycastle_bcprov_jdk15on",
      artifact = "org.bouncycastle:bcprov-jdk15on:1.57",
      sha1 = "f66a135611d42c992e5745788c3f94eb06464537",
  )


  # org.bouncycastle:bcmail-jdk15on:jar:1.57
  native.maven_jar(
      name = "org_bouncycastle_bcpkix_jdk15on",
      artifact = "org.bouncycastle:bcpkix-jdk15on:1.57",
      sha1 = "5c96e34bc9bd4cd6870e6d193a99438f1e274ca7",
  )




def generated_java_libraries():
  native.java_library(
      name = "org_bouncycastle_bcmail_jdk15on",
      visibility = ["//visibility:public"],
      exports = ["@org_bouncycastle_bcmail_jdk15on//jar"],
      runtime_deps = [
          ":org_bouncycastle_bcpkix_jdk15on",
          ":org_bouncycastle_bcprov_jdk15on",
      ],
  )


  native.java_library(
      name = "org_bouncycastle_bcprov_jdk15on",
      visibility = ["//visibility:public"],
      exports = ["@org_bouncycastle_bcprov_jdk15on//jar"],
  )


  native.java_library(
      name = "org_bouncycastle_bcpkix_jdk15on",
      visibility = ["//visibility:public"],
      exports = ["@org_bouncycastle_bcpkix_jdk15on//jar"],
      runtime_deps = [
          ":org_bouncycastle_bcprov_jdk15on",
      ],
  )


