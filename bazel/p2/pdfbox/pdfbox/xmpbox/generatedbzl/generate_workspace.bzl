# The following dependencies were calculated from:
#
# generate_workspace --maven_project=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/xmpbox -o=/Users/taomo/Documents/workspace/bazelProjects/pdfbox/pdfbox/xmpbox/generatedbzl


def generated_maven_jars():
  # org.apache.pdfbox:xmpbox:bundle:3.0.0-SNAPSHOT
  # pom.xml got requested version
  native.maven_jar(
      name = "commons_logging_commons_logging",
      artifact = "commons-logging:commons-logging:1.2",
      sha1 = "4bfc12adfe4842bf07b657f0369c4cb522955686",
  )


  # junit:junit:jar:4.12
  native.maven_jar(
      name = "org_hamcrest_hamcrest_core",
      artifact = "org.hamcrest:hamcrest-core:1.3",
      sha1 = "42a25dc3219429f0e5d060061f71acb49bf010a0",
  )


  # org.apache.pdfbox:xmpbox:bundle:3.0.0-SNAPSHOT
  # pom.xml got requested version
  native.maven_jar(
      name = "junit_junit",
      artifact = "junit:junit:4.12",
      sha1 = "2973d150c0dc1fefe998f834810d68f278ea58ec",
  )




def generated_java_libraries():
  native.java_library(
      name = "commons_logging_commons_logging",
      visibility = ["//visibility:public"],
      exports = ["@commons_logging_commons_logging//jar"],
  )


  native.java_library(
      name = "org_hamcrest_hamcrest_core",
      visibility = ["//visibility:public"],
      exports = ["@org_hamcrest_hamcrest_core//jar"],
  )


  native.java_library(
      name = "junit_junit",
      visibility = ["//visibility:public"],
      exports = ["@junit_junit//jar"],
      runtime_deps = [
          ":org_hamcrest_hamcrest_core",
      ],
  )


