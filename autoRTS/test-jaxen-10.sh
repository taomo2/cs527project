#!/bin/bash

# Measure the last 10 commits of Jaxen.

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="jaxen"

source java-config.sh

./update-testlib.sh
./clean-project.sh

set +e

run_tests()
{
	# Checkout the commit.
	./checkout-${PROJECT}.sh $1

	# Run test selector.
	DV="../lib/testtool.jar"
	(cd "$PROJECT"; java -DlogLevel=INFO -jar "$DV" compile -project . >> log.tmp 2>&1)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; java -DlogLevel=INFO -jar "$DV" runtests -project . 2>&1 | tee -a log.tmp | grep "Running tests:")
		(cd "$PROJECT"; java -jar "$DV" runall -project . >> log.tmp 2>&1)
	fi
}

# First commit is run and discarded (need starting point for measurement).
run_tests b3197dcbd525da04c136d6a9908ea7ec5196d9ea

# Run 10 commits in sequence:
run_tests 36f35a1e2372e2bea7a9edc17c86cf895e17dc2f
run_tests 97c2ca38c77af90b877c698e7c0738ac637e11b5
run_tests d44930aa56eedea376998548131cdc8a410ebf5e
run_tests 251886b0b192114894e32d90bac59be7b994d59c
run_tests 9fcf08f10cef66e97ee601df493b6498580e05b6
run_tests bb1d4232b02de7fb49dcf873ec15ddf068a7bbfa
run_tests fad134cccf8776b38b31959bed29fe066a52850d
run_tests 175e3610b0e2c09d6192832a92c4d05b4edae20f
run_tests 640c0a5ca3d3c20aacb589079e39113cf962ed43
run_tests 58a9242eb8acf70891039e6cb8eb9918c4d0b832

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "log10-${PROJECT}.${TIMESTAMP}"
gzip "log10-${PROJECT}.${TIMESTAMP}"
echo "Benchmark run completed Output file: log10-${PROJECT}.${TIMESTAMP}.gz"
