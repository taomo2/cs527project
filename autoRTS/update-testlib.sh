#!/bin/sh

# Update autotester to the latest version.

TOOLJAR="/Users/taomo/Documents/workspace/autorts/launcher/build/libs/launcher.jar"
if [ ! -e "$TOOLJAR" ]; then
	echo "Error: AutoRTS Jar file not at $TOOLJAR!"
	exit 1
fi
cp "$TOOLJAR" lib/testtool.jar
