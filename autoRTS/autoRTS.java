import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class autoRTS {
	public class Commit{
		String name;
		HashSet<String> testSelected = new HashSet<>();
		
		public Commit(String name) {
			this.name = name;
		}
		
		public void addTest(String testName){
			testSelected.add(testName);
		}
		
		public int getTestSize(){
			return testSelected.size();
		}
		
	}
	
	
	public static void main(String [] args) {

		
		final String [] PROJECTS = {"cli","dbutils","fileupload","functor","io","lang"};
		
		String projectName = PROJECTS[5];
		
        String autoRTSfileName = "resources/"+ projectName +"TestAutoRTS.txt";
        String startsfileName = "resources/"+ projectName +"TestSTARTS.txt";

        List<Commit> autoRTSCommits = new ArrayList<>();
        List<Commit> startsCommits = new ArrayList<>();
        
        getListAutoRTS(autoRTSfileName, autoRTSCommits);
        getListStarts(startsfileName, startsCommits);
        
        compareToCSV(projectName, autoRTSCommits , startsCommits);
    }

	public static void compareToCSV(String project, List<Commit> autoRTS, List<Commit> starts) {
		String COMMA_DELIMITER = ",";
		String NEW_LINE_SEPARATOR = "\n";
		String FILE_HEADER = "PROJECT,SHA,STARTS,AUTORTS,STARTS-AUTORTS,AUTORTS-STARTS";
		
		FileWriter fileWriter = null;
		
		try {
			fileWriter = new FileWriter(project+".csv");
			fileWriter.append(FILE_HEADER.toString());
			fileWriter.append(NEW_LINE_SEPARATOR);
			
			if (autoRTS.size() != starts.size()) {
				System.out.println("Size is not the same!!");
				return;
			}
			
			for (int i = 0; i < autoRTS.size() ; i++){
				if (!autoRTS.get(i).name.equals(starts.get(i).name)) {
					System.out.println("Commit not the same!!!");
					return;
				}
				
				HashSet<String> startsTemp = starts.get(i).testSelected;
				HashSet<String> autoRTSTemp = autoRTS.get(i).testSelected;
				
				fileWriter.append(project);
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(autoRTS.get(i).name.substring(9));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(startsTemp.size()+"");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(autoRTSTemp.size()+"");
				fileWriter.append(COMMA_DELIMITER);
				
				
				
				int start_autoRTS = 0;
				int autoRTS_starts = 0;
				for (String temp : startsTemp){
					if (!autoRTSTemp.contains(temp)){
						System.out.println(temp);
						start_autoRTS++;
					}
				}
				
				for (String temp : autoRTSTemp){
					if (!startsTemp.contains(temp)){
						autoRTS_starts++;
					}
				}
				
				fileWriter.append(start_autoRTS+"");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(autoRTS_starts+"");
				fileWriter.append(COMMA_DELIMITER);
				
				fileWriter.append(NEW_LINE_SEPARATOR);
			}
			
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (Exception e2) {
				System.out.println("Error while flushing/closing fileWriter !!!");
			}
		}
	}
	

	public static void getListAutoRTS(String fileName, List<Commit> commits) {
		autoRTS porcessor = new autoRTS();
		
		String line;
		try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = 
                new FileReader(fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);

            Commit tempCommit = null;
            
            
            while((line = bufferedReader.readLine()) != null) {
                if (line.startsWith("Checkout")) {
                	if (tempCommit!=null) {
                		commits.add(tempCommit);
                	}
                	tempCommit = porcessor.new Commit(line);
                	
                }
                
                //for autoRTS
                if (line.startsWith("Selected test:")) {
                	tempCommit.addTest(line.substring(15));
                }
                
            }   
            if (tempCommit!=null) {
        		commits.add(tempCommit);
        	}
            
            System.out.println("total commits:"+commits.size());
            // Always close files.
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");                  
        }
	}
	
	public static void getListStarts(String fileName, List<Commit> commits) {
		autoRTS porcessor = new autoRTS();
		
		String line;
		
		boolean start = false;
		
		try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = 
                new FileReader(fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);

            Commit tempCommit = null;
            
            
            while((line = bufferedReader.readLine()) != null) {
                if (line.startsWith("Checkout")) {
                	if (tempCommit!=null) {
                		commits.add(tempCommit);
                		System.out.println(tempCommit.getTestSize());
                	}
                	tempCommit = porcessor.new Commit(line);
                	
                }
                
                //for starts
                if (line.startsWith("INFO: ********** AffectedTests **********")) {
                	start = true;
                	continue;
                }
                
                if (line.startsWith("INFO: ALL(count):") || line.startsWith("INFO: AffectedTests found no classes.") || line.startsWith("INFO: STARTS")) {
                	start = false;
                }
                
                if (start){
                	tempCommit.addTest(line.substring(6));
                }
                
            }   
            if (tempCommit!=null) {
        		commits.add(tempCommit);
        		System.out.println(tempCommit.getTestSize());
        	}
            
            System.out.println("total commits:"+commits.size());
            // Always close files.
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");                  
        }
	}
}
