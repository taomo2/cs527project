#!/bin/bash

# Measure the last 40 commits N number of times.

# Number of measurement series:
N=1

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="commons-dbutils"

source java-config.sh

./update-testlib.sh
./clean-project_starts.sh

set +e

run_tests()
{
	# Checkout the commit.
	./checkout-${PROJECT}.sh $1

	cp ./insert.sh "$PROJECT"
	# run test selector
	(cd "$PROJECT"; ./insert.sh)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; mvn starts:select 2>&1 | tee -a log.tmp)
		(cd "$PROJECT"; mvn starts:starts 2>&1 | tee -a log.tmp)
	fi
}

export TEST_JAVA_HOME="${JAVA_SE7_HOME}"

for i in `seq 1 $N`; do
	if [ -e "${PROJECT}/dependencies.map" ]; then
		rm "${PROJECT}/dependencies.map"
	fi

	echo "Starting ${PROJECT} series $i"

	# First commit is run and discarded (need starting point for measurement).
	END_COMMIT="084f286a0d7ffdde482ab5a9e4ada7affe2547f0"

	# Run N commits with the last one being END_COMMIT:
	for COMMIT in $(cd "${PROJECT}"; git rev-list --reverse "${END_COMMIT}" | tail -n2); do
		run_tests "${COMMIT}"
	done

done

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "logN-${PROJECT}.${TIMESTAMP}.starts"
gzip "logN-${PROJECT}.${TIMESTAMP}.starts"
