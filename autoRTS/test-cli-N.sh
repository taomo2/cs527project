#!/bin/bash

# Measure the last 40 commits N number of times.

# Number of measurement series:
N=1

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="commons-cli"

source java-config.sh

./update-testlib.sh
./clean-project_starts.sh

set +e

run_tests()
{
	# Checkout the commit.
	./checkout-${PROJECT}.sh $1

	# Run test selector.
	DV="../lib/testtool.jar"
	(cd "$PROJECT"; java -DlogLevel=INFO -jar "$DV" compile -project . >> log.tmp 2>&1)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; java -DlogLevel=INFO -jar "$DV" runtests -project . 2>&1 | tee -a log.tmp | grep "Running tests:")
		(cd "$PROJECT"; java -jar "$DV" runall -project . >> log.tmp 2>&1)
	fi
}

export TEST_JAVA_HOME="${JAVA_SE7_HOME}"

for i in `seq 1 $N`; do
	if [ -e "${PROJECT}/dependencies.map" ]; then
		rm "${PROJECT}/dependencies.map"
	fi

	echo "Starting ${PROJECT} series $i"

	# First commit is run and discarded (need starting point for measurement).
	END_COMMIT="2392ae8afc08b496a3eb49908aa421ea86a9679e"

	# Run N commits with the last one being END_COMMIT:
	for COMMIT in $(cd "${PROJECT}"; git rev-list --reverse "${END_COMMIT}" | tail -n20); do
		run_tests "${COMMIT}"
	done

done

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "logN-${PROJECT}.${TIMESTAMP}"
gzip "logN-${PROJECT}.${TIMESTAMP}"
