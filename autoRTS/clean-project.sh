#!/bin/bash

# Commons Functor is at git://git.apache.org/commons-functor.git
# GS Collections is at https://github.com/goldmansachs/gs-collections.git
# Closure compiler is at https://github.com/google/closure-compiler.git
# PdfBox is at git://git.apache.org/pdfbox.git

if [ ! -d "$PROJECT" ]; then
	git clone "https://bitbucket.org/joqvist/ts${PROJECT}.git" "${PROJECT}"
	if [ "$?" -ne "0" ]; then
		exit $?
	fi
else
	(cd "${PROJECT}"; git clean -fd > /dev/null)
	(cd "${PROJECT}"; git clean -fX > /dev/null)
	if [ "$?" -ne "0" ]; then
		exit $?
	fi
fi

