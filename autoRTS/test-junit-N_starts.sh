#!/bin/bash

# Measure the last 40 commits N number of times.

# Number of measurement series:
N=1

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="junit"

source java-config.sh

./update-testlib.sh
./clean-project.sh

set +e

run_tests()
{
	# checkout the commit
	./checkout-${PROJECT}.sh $1

	cp ./insert.sh "$PROJECT"
	# run test selector
	(cd "$PROJECT"; ./insert.sh)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; mvn starts:select 2>&1 | tee -a log.tmp)
		(cd "$PROJECT"; mvn starts:starts 2>&1 | tee -a log.tmp)
	fi
}

for i in `seq 1 $N`; do
	echo "Starting ${PROJECT} series $i"

	# first commit is run and discarded (need starting point for measurement
	run_tests 5e2616aa6280909649185240e0194fd14c5147a8
	run_tests 304a03ec5e57f52d9c5490a7de599629f2de3cf7
done

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "logN-${PROJECT}.${TIMESTAMP}.starts"
gzip "logN-${PROJECT}.${TIMESTAMP}.starts"

