#!/bin/bash

set -eu

if [ $# -lt "1" ]; then
    echo "Usage: checkout GITHASH"
    exit 1
fi

if [ ! -d "$PROJECT" ]; then
	echo "Error: no such directory: $PROJECT"
	echo "Make sure that the clean-project script is run first!"
	exit 1
fi

cd "$PROJECT"
echo "Checkout $1" | tee -a log.tmp
git reset --hard $1 > /dev/null
if [ "$?" -ne "0" ]; then
	exit $?
fi

LIB='mvnlibs'
if [ -d "$LIB" ]; then
	rm -r "$LIB"
fi

if [ -e "pom.xml" ]; then
	# Download maven dependencies.
	mkdir "${LIB}"
	mvn org.apache.maven.plugins:maven-dependency-plugin:2.1:copy-dependencies -DoutputDirectory="$(readlink -f ${LIB})"

	# Remove the commons-functor library because we do not need it.
	rm -f "${LIB}"/commons-functor-api-*

	# Build classpath.
	CLASSPATH=$(JARS=("$LIB"/*.jar); IFS=:; echo "${JARS[*]}")
else
	CLASSPATH="../lib/junit-3.7.jar"
fi

# Build test selector configuration:
if [ -d "core" ]; then
	echo "classpath.src=core/src/main/java\:core/src/test/java\:api/src/main/java" > dependencies.cfg
else
	if [ -d "src/main" ]; then
		echo "classpath.src=src/main/java\:src/test/java" > dependencies.cfg
	else
		echo "classpath.src=src/java\:src/test" > dependencies.cfg
	fi
fi
echo "classpath.lib=${CLASSPATH//:/\\:}" >> dependencies.cfg
echo "classpath.con=" >> dependencies.cfg
echo "java.home=${TEST_JAVA_HOME}" >> dependencies.cfg
echo "bootclasspath=${TEST_JAVA_HOME}/lib/rt.jar" >> dependencies.cfg
if [[ $CLASSPATH =~ junit-3 ]] ; then
	echo "testRunner=JUNIT3" >> dependencies.cfg
else
	echo "testRunner=JUNIT4" >> dependencies.cfg
fi
