#!/bin/bash

function insertAfter # file line newText
{
   local file="$1" line="$2" newText="$3"
   sed -i -e "/^$line$/a"$'\\\n'"$newText"$'\n' "$file"
}

if grep -Fxq "<artifactId>starts-maven-plugin</artifactId>" pom.xml
then
    :
else
    insertAfter pom.xml " *<plugins>" "    </plugin>"
	insertAfter pom.xml " *<plugins>" "      <version>1.3</version>"
	insertAfter pom.xml " *<plugins>" "      <artifactId>starts-maven-plugin</artifactId>"
	insertAfter pom.xml " *<plugins>" "      <groupId>edu.illinois</groupId>"
	insertAfter pom.xml " *<plugins>" "    <plugin>"
fi

