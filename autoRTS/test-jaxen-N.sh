#!/bin/bash

# Measure the last 40 commits N number of times.

# Number of measurement series:
N=1

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="jaxen"

source java-config.sh

./update-testlib.sh
./clean-project.sh

set +e

run_tests()
{
	# Checkout the commit.
	./checkout-${PROJECT}.sh $1

	# Apply Ekstazi patch.
	#(cd "${PROJECT}"; git apply "../jaxen-ekstazi.patch")

	# Run test selector.
	DV="../lib/testtool.jar"
	(cd "$PROJECT"; java -DlogLevel=INFO -jar "$DV" compile -project . >> log.tmp 2>&1)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; java -DlogLevel=INFO -jar "$DV" runtests -project . 2>&1 | tee -a log.tmp | grep "Running tests:")
		(cd "$PROJECT"; java -jar "$DV" runall -project . >> log.tmp 2>&1)
	fi

	# Backup map for comparison.
	#cp "${PROJECT}/dependencies.map" "${PROJECT}/${1}.map"
}

export TEST_JAVA_HOME="${JAVA_SE7_HOME}"

for i in `seq 1 $N`; do
	if [ -e "${PROJECT}/dependencies.map" ]; then
		rm "${PROJECT}/dependencies.map"
	fi

	echo "Starting ${PROJECT} series $i"

	# First commit is run and discarded (need starting point for measurement.
	run_tests 469d4cb3af010dbc690b645a34388b2f537b3376
	run_tests 7f49532cf441829da57cb59116c92841a9556086
	run_tests b111f68e4ce89b9420d02aa645746db08371788e
	run_tests bd0a8605bf9c43457d54a9f77739706328197938
	run_tests 5feacbb69d6ae0d4a7f0abafe9310c2617d61c5b
	run_tests 102640396ae79205ed5288ca97e2f3e51df4ea20
	run_tests 397be3ee484fc3bb717987a2fd4cfe6e51a0b6dc
	run_tests 8a7f6ccf8e7d6030737848eaf06bcfebc39eb8b0
	run_tests c0cabcf5f594570f615bc021615034fb775a76f3
	run_tests 99799e8d50d0792f3f2b9d277ee43f5ea14e0687
	run_tests 5d61d8982d7d7061e167bbfef90b9c863783dfd2
	run_tests 519cfb0d8c4d7a7b58e6e8f1186791192767cef7
	run_tests 619900ff02231b8d64ba28120c58360525b9567a
	run_tests ef00a96c7b0f0dbacbd634cc6a3bde46f666a009
	run_tests d7b7445b62a10d3bd7ed3fa27137bde9cdba6c98
	run_tests ed7f7f405bc75be9310559a1c476e5b4f41282e2
	run_tests 63fbfd1f3adb5e5a8f329affe156c8abe01b238a
	run_tests c518611a5c1920178354dcd01ccbc99871bfad8e
	run_tests 9b2a14457b008c58681b4e0deb3aebe79de7d14c
	run_tests 1b97d204ebdbc495adcb1d2bff1b493e68b08a39
	run_tests 29b6d9e6807f8aa0c06b310dfb282a42c7939a7c
	run_tests 1e24d3a690edf82ff2227fd95c8c9c3c5d1d58de
	run_tests 1b03db00c42a913faeb85c4bc681b43d0d69db3e
	run_tests 4b0f5e26c798a69e3d974753796593cb757f9a87
	run_tests 697ad815fd801fdbb0fa837d64f0457166722048
	run_tests 1aec325f39a97613e10f6549a6ce1299af4c4a5a
	run_tests 2ca96201e3c74f5e0c8a0a8dcba4a4d03fde12ac
	run_tests d252f831cc18519bbe3d51ea1894200cc5ec1729
	run_tests f70efefe5fb0eea2a0fcfd80359529450ff33b42
	run_tests 8243e63340c3ba9439aec2196cf0772ca28da449
	run_tests b3197dcbd525da04c136d6a9908ea7ec5196d9ea
	run_tests 36f35a1e2372e2bea7a9edc17c86cf895e17dc2f
	run_tests 97c2ca38c77af90b877c698e7c0738ac637e11b5
	run_tests d44930aa56eedea376998548131cdc8a410ebf5e
	run_tests 251886b0b192114894e32d90bac59be7b994d59c
	run_tests 9fcf08f10cef66e97ee601df493b6498580e05b6
	run_tests bb1d4232b02de7fb49dcf873ec15ddf068a7bbfa
	run_tests fad134cccf8776b38b31959bed29fe066a52850d
	run_tests 175e3610b0e2c09d6192832a92c4d05b4edae20f
	run_tests 640c0a5ca3d3c20aacb589079e39113cf962ed43
	run_tests 58a9242eb8acf70891039e6cb8eb9918c4d0b832
done

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "logN-${PROJECT}.${TIMESTAMP}"
gzip "logN-${PROJECT}.${TIMESTAMP}"

