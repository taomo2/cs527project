#!/bin/bash

# Measure the last 40 commits N number of times.

# Number of measurement series:
N=2

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="junit"

source java-config.sh

./update-testlib.sh
./clean-project.sh

set +e

run_tests()
{
	# checkout the commit
	./checkout-${PROJECT}.sh $1

	# run test selector
	DV="../lib/testtool.jar"
	(cd "$PROJECT"; java -DlogLevel=INFO -jar "$DV" compile -project . >> log.tmp 2>&1)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; java -DlogLevel=INFO -jar "$DV" runtests -project . 2>&1 | tee -a log.tmp | grep "Running tests:")
		(cd "$PROJECT"; java -jar "$DV" runall -project . >> log.tmp 2>&1)
	fi
}

for i in `seq 1 $N`; do
	if [ -e "${PROJECT}/dependencies.map" ]; then
		rm "${PROJECT}/dependencies.map"
	fi

	echo "Starting ${PROJECT} series $i"

	# first commit is run and discarded (need starting point for measurement
	run_tests 5e2616aa6280909649185240e0194fd14c5147a8
	run_tests 304a03ec5e57f52d9c5490a7de599629f2de3cf7
done

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "logN-${PROJECT}.${TIMESTAMP}"
gzip "logN-${PROJECT}.${TIMESTAMP}"

