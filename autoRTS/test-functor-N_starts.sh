#!/bin/bash

# Measure the last 20 commits N number of times.

# Number of measurement series:
N=1

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="functor"

source java-config.sh

./update-testlib.sh
./clean-project.sh

set +e

run_tests()
{
	# Checkout the commit.
	./checkout-${PROJECT}.sh $1

	cp ./insert.sh "$PROJECT"
	# run test selector
	(cd "$PROJECT"; ./insert.sh)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; mvn starts:select 2>&1 | tee -a log.tmp)
		(cd "$PROJECT"; mvn starts:starts 2>&1 | tee -a log.tmp)
	fi
}

for i in `seq 1 $N`; do

	echo "Starting ${PROJECT} series $i"

	run_tests 5de509a579b0c39b1dcd3c7641680ade54851c3b
	run_tests 4c73da5e2272702521bb61e5b4a94c3e8f4547f9
	run_tests d6beb0438ed3432cd4246a62b821735540b45431
	run_tests 56035111e4879168106d68786042538462220765
	run_tests 10c78767a3cc6937733c3a85ea20b6835c5d9bfc
	run_tests 0bb123752e2f7c225a5322d9f00cfd6df1a75510
	run_tests 64dfb78b1f5d5fd7cfcd8c8c5c3b0f58eba09d0a
	run_tests 1eec0a2f3ad3180173478a54b8ca12bca459a844
	run_tests 1004abda7fe5638860661f4cbe81be2e4bddf90c
	run_tests 5db233e4c3e2e64bb39b07fa501943839b875d3f
	run_tests a4c8160726a1abb87560c568861719dfe3698379
	run_tests 9935540e7415f2c78e512eba79e79af92fd79b2f
	run_tests 836eea72d53575a750d3ae56566b895df9eebbb2
	run_tests 38720128bc84de1535a7844f106f7abf8e2576e1
	run_tests 2941c23f509d3a306d6f22aa8ebf912c70b66528
	run_tests 773002133be1105d2d9d0f4d531b2dc6d1872e35
	run_tests 6f0efcb48d419651838607bbaf098bbb464b9185
	run_tests 7f0a739dff9c4598f916b7ef530a6363e701a7d0
	run_tests 8771fb2958eae862b79462524cf3be5dfbf8fb9c
	run_tests 821419684fe450f9c36de15ccd056c7c4f38d2e4
	run_tests 3da1a4b1214428c04ca9102ef4567ba61b0ff5be
done

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "log-${PROJECT}.${TIMESTAMP}.starts"
gzip "log-${PROJECT}.${TIMESTAMP}.starts"

