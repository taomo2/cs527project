#!/bin/bash

if [ $# -lt "1" ]; then
    echo "Usage: checkout GITHASH"
    exit 1
fi

if [ ! -d "$PROJECT" ]; then
	echo "Error: no such directory: $PROJECT"
	echo "Make sure that the clean-project script is run first!"
	exit 1
fi

cd "$PROJECT"
echo "Checkout $1" | tee -a log.tmp
git reset --hard $1 > /dev/null
if [ "$?" -ne "0" ]; then
	exit $?
fi

JUNIT="junit-4.12.jar"
IO="commons-io-2.6.jar"
ANIMAL="animal-sniffer-maven-plugin-1.16.jar"
SNIFFER="animal-sniffer-1.16.jar"
ASM="asm-all-5.2.jar"
BOOT="java-boot-classpath-detector-1.16.jar"


getlib()
{
	if [ ! -e "$1" ]; then
		cp "../lib/$1" .
	fi
}

getlib "$JUNIT"
getlib "$IO"
getlib "$ANIMAL"
getlib "$SNIFFER"
getlib "$ASM"
getlib "$BOOT"


CLASSPATH="${JUNIT}:${IO}:${ANIMAL}:${SNIFFER}:${ASM}:${BOOT}"

#CLASSPATH="${JUNIT}"

if [ -d "src/main/java" ]; then
	echo "classpath.src=src/main/java\:src/test/java" > dependencies.cfg
else
	echo "classpath.src=src/java\:src/test" > dependencies.cfg
fi
echo "classpath.lib=${CLASSPATH//:/\\:}" >> dependencies.cfg
echo "classpath.con=" >> dependencies.cfg
echo "java.home=${TEST_JAVA_HOME}" >> dependencies.cfg
echo "bootclasspath=${TEST_JAVA_HOME}/lib/rt.jar" >> dependencies.cfg
echo "testRunner=JUNIT4" >> dependencies.cfg
echo "javac.flags=-encoding latin1" >> dependencies.cfg

