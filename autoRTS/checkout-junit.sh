#!/bin/bash

if [ $# -lt "1" ]; then
    echo "Usage: checkout GITHASH"
    exit 1
fi

if [ ! -d "$PROJECT" ]; then
	echo "Error: no such directory: $PROJECT"
	echo "Make sure that the clean-project script is run first!"
	exit 1
fi

cd "$PROJECT"
echo "Checkout $1" | tee -a log.tmp
git reset --hard $1 > /dev/null
if [ "$?" -ne "0" ]; then
	exit $?
fi

if [ -d "src" ]; then
	echo "classpath.src=src" > dependencies.cfg
else
	# early commits have the source directly in project root
	echo "classpath.src=." > dependencies.cfg
fi

LIB="lib"
if [ -d "$LIB" ]; then
	CLASSPATH=$(JARS=("$LIB"/*.jar); IFS=:; echo "${JARS[*]}")
	echo "classpath.lib=${CLASSPATH//:/\\:}" >> dependencies.cfg
fi

echo "classpath.con=" >> dependencies.cfg
echo "java.home=${TEST_JAVA_HOME}" >> dependencies.cfg
echo "bootclasspath=${TEST_JAVA_HOME}/jre/lib/rt.jar" >> dependencies.cfg
echo "testRunner=${JUNIT_HARNESS_VERSION}" >> dependencies.cfg
echo "javac.flags=-encoding latin1" >> dependencies.cfg

