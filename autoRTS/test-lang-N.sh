#!/bin/bash

# Measure the last 40 commits N number of times.

# Number of measurement series:
N=1

interrupted()
{
    exit $?
}

trap interrupted SIGINT

set -eu

export PROJECT="lang"

source java-config.sh

./update-testlib.sh
./clean-project.sh

set +e

run_tests()
{
	# Checkout the commit.
	./checkout-${PROJECT}.sh $1

	# Run test selector.
	DV="../lib/testtool.jar"
	(cd "$PROJECT"; java -DlogLevel=INFO -jar "$DV" compile -project . >> log.tmp 2>&1)
	if [ "$?" -eq "0" ]; then
		(cd "$PROJECT"; java -DlogLevel=INFO -jar "$DV" runtests -project . 2>&1 | tee -a log.tmp | grep "Running tests:")
		(cd "$PROJECT"; java -jar "$DV" runall -project . >> log.tmp 2>&1)
	fi
}

export TEST_JAVA_HOME="${JAVA_SE7_HOME}"

for i in `seq 1 $N`; do
	if [ -e "${PROJECT}/dependencies.map" ]; then
		rm "${PROJECT}/dependencies.map"
	fi

	echo "Starting ${PROJECT} series $i"

	# First commit is run and discarded (need starting point for measurement).
	run_tests bb5cbd0c0802ee07569803cc5537241c0d2a92e2
	run_tests 74fdc35a9852e1f33043528391a376dea90725be
	run_tests d6f31a650d07f0daf273d1bbf51ffdac93cd2cae
	run_tests 7d40ae9741d994803583dcc98fe18f20a8bcb8f7
	run_tests 0f6a1aba67114ffbca39e9085b9bc0e25b3d7b86
	run_tests 6290347ab5958106aa5c6d13abf2b22a1a26fc67
	run_tests 725c290cd0a747041ab44259b87c476b470827e0
	run_tests 33fe37c8b790bac572c34bcdc39f148799c2ada0
	run_tests 109e37f189129e415791c1d609e4951aa37571ef
	run_tests 5557b9a23e2971ea31e852857db61cd332b26706
	run_tests 7ac8e4cd7a44d2d99ea2322839a2275f40f0b7b1
	run_tests 4d43f048c53571b71720ebefc582ba5d5197bd5d
	run_tests 8b8ce704302aaaa099a04c477e8f25f2f76bcc27
	run_tests 3de103dd72b9b5af42fd604c548047a5dd1e246b
	run_tests 48a6305be2b9358b7df9cdc7c23ce589e5940ace
	run_tests dd8f44884d06617322ae1e5a6baa0f9c8d343065
	run_tests aff6ca747e6c19400353a658f08fba3ece38e9ef
	run_tests 3482322192307199d4db1c66e921b7beb6e6dcb6
	run_tests 54ebf4f856ded17088adb21b741f3fdb6fd3dd05
	run_tests 7cf48fbeec5c810bddf33753aac105ebf3bd6ff6
	run_tests efa1ea4994180647a88f528ab15174c3a1c8def7
	run_tests 9fc8928943bb9d7e3c345edc5e0c0585bc7abce9
	run_tests ede44e5be976b9854d0886a7c93e3ea784ec2976
	run_tests 772a65e7bb020e389768376e0df86c10bf616f55
	run_tests 4bab3ab1d9e49f532006334b2c05fa1b953fa98c
	run_tests 5791577c91c78d4b8643e824365103451ec24f26
	run_tests beb36db500ae083d9976a50f4521ea5af12dac63
	run_tests c82f58e58ace1b88b40d2e05ee6017da52522b23
	run_tests cf4edea49a1431dbdff659f0069ac16ce1b21259
	run_tests 2dae9c0c08f510fa031381fc9fb7308b8356963f
	run_tests c8e6ce5c6adbb256b9eac50b6193a879745ab4e9
	run_tests 192e8765405155f2796c3b8343e100567147d536
	run_tests 5a6bdc0b47477bdb83e64925c77579b5be4f9222
	run_tests 83ad5f8bcdca8df4086226b13f3ceeecca7ed358
	run_tests e47f8b310edd2fdb78843b06608ae0e2b9b6ea66
	run_tests 8c732c52ddc7dbf9516676bab4f3513e8b1df3e7
	run_tests 4769e7b4f007e4c02f396934e14fdf6473be6248
	run_tests 1048346e0655bdef22c9820e96bb77d34b3a69d1
	run_tests 666a3f4c5426ead0304a4831a50872321e7becc7
	run_tests 6d01210ac6312c08cf9d64af1316d17efaeac8ba
	run_tests ba18c038e8e4f340d699e086b97052577d5c4671
done

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")
cp "${PROJECT}/log.tmp" "logN-${PROJECT}.${TIMESTAMP}"
gzip "logN-${PROJECT}.${TIMESTAMP}"
